import Aesop

example: α → α := by aesop

inductive MyList (α: Type _)
  | nil
  | cons (hd: α) (tl: MyList α)

namespace MyList

protected def append: MyList α → MyList α → MyList α
  | nil, ys => ys
  | cons x xs, ys => cons x (MyList.append xs ys)

instance: Append (MyList α) := ⟨MyList.append⟩

@[simp]
theorem nil_append: nil ++ xs = xs := rfl

@[simp]
theorem cons_append: cons x xs ++ ys = cons x (xs ++ ys) := rfl

declare_aesop_rule_sets [MyList.NonEmpty]

@[aesop safe (rule_sets [MyList.NonEmpty]) [constructors, cases]]
inductive NonEmpty: MyList α → Prop
  | cons: NonEmpty (cons x xs)

@[aesop safe apply]
theorem NonEmpty.append {xs: MyList α}:
  NonEmpty xs → (ys: _) → NonEmpty (xs ++ ys)
  := by aesop (rule_sets [MyList.NonEmpty])
  
@[aesop norm]
theorem append_nil {xs : MyList α} :
    xs ++ nil = xs := by
  induction xs <;> aesop

theorem append_assoc {xs ys zs : MyList α} :
    (xs ++ ys) ++ zs = xs ++ (ys ++ zs) := by
  induction xs <;> aesop

inductive BinT (α: Type _)
  | leaf
  | branch (data: α) (left right: BinT α)

inductive BinTIso: BinT α -> BinT β -> Type
  | leaf: BinTIso BinT.leaf BinT.leaf
  | branch {l r l' r' d d'}: 
    BinTIso l l' -> 
    BinTIso r r' ->
    BinTIso (BinT.branch d l r) (BinT.branch d' l' r') 

def tree_product_standard (l: BinT α) (r: BinT β) (I: BinTIso l r) 
  : BinT (α × β)
  := 
  match l, r with
  | BinT.leaf, BinT.leaf => BinT.leaf
  | BinT.branch a ll lr, BinT.branch b rl rr 
    => BinT.branch (a, b) 
      (tree_product_standard ll rl (by cases I; assumption)) 
      (tree_product_standard lr rr (by cases I; assumption))
  --| BinT.leaf, BinT.branch b rl rr => sorry

--TODO: go report this as a bug
def tree_product (l : BinT α) (r: BinT β): BinTIso l r -> BinT (α × β)
  | BinTIso.leaf => BinT.leaf
  | @BinTIso.branch _ _ _ _ _ _ a b l r => BinT.branch (a, b) 
    (tree_product _ _ l) 
    (tree_product _ _ r)